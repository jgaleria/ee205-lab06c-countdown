///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Date October 18, 2002 10:18
//
// Joshua Galeria jgaleria@hawaii.edu
// 2/18/2022
///////////////////////////////////////////////////////////////////////////////

//Header
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>

//main
int main() {
   
   //defining struct 
   struct tm {
      int tm_sec;  //seconds
      int tm_min;  //minutes
      int tm_hour; //hours
      int tm_mday; //day of the month
      int tm_mon;  //month
      int tm_year; //number of years since 1900
   };

   //declaring
   struct tm new;
   //int current;

   //October 18, 2002 10:18:10
   new.tm_sec  = 10;
   new.tm_min  = 18;
   new.tm_hour = 10;
   new.tm_mday = 18;
   new.tm_mon  = 10;
   new.tm_year = 2002 - 1900;
   
   time_t referenceTime = mktime(&new);
   
   //current = mktime(&new);

   //test 
   //referenceTime = 123192317349872398;

   //Hard Coded Reference time
   printf("Reference time: Fri, Oct 18 10:18:10 PM HST 2002\n");

   //Loop to print out countdown
   while( true ) {
      printf("Years: [%ld] Days: [%ld] Hours: [%ld] Minutes: [%ld] Seconds: [%ld]\n",
            referenceTime % 31536000,
            referenceTime % 86400,
            referenceTime % 3600,
            referenceTime % 60,
            referenceTime);
      referenceTime--;
      sleep(1);
   } 

   //return
   printf("Time's up\n");
   return 0;
}
